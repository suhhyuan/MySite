package my.common;

import com.jfinal.kit.JsonKit;

import java.util.HashMap;

/**
 * Created by Administrator on 2017/5/25.
 */
public class Util {
    public static String ajaxJson(boolean success, String message) {
        HashMap<String, Object> json = new HashMap<String, Object>();
        json.put("success", success);
        json.put("message", message);
        return JsonKit.toJson(json);
    }

    public static String ajaxSuccess(String message) {
        return ajaxJson(true, message);
    }

    public static String ajaxFail(String message) {
        return ajaxJson(false, message);
    }


}

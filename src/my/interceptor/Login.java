package my.interceptor;

import com.jfinal.aop.Interceptor;
import com.jfinal.aop.Invocation;
import my.common.Util;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by Administrator on 2017/4/6 0006.
 */
public class Login implements Interceptor {
    @Override
    public void intercept(Invocation invocation) {

        if (invocation.getController().getSessionAttr("user") == null) {
            HttpServletRequest request = invocation.getController().getRequest();
            String header = request.getHeader("x-requested-with");
            if (header != null && header.equalsIgnoreCase("XMLHttpRequest")) {
                invocation.getController().renderJson(Util.ajaxFail("没有登录"));
            } else {
                invocation.getController().redirect("/login");
            }


        } else
            invocation.invoke();
    }
}

-- --------------------------------------------------------
-- 主机:                           127.0.0.1
-- 服务器版本:                        5.5.47 - MySQL Community Server (GPL)
-- 服务器操作系统:                      Win32
-- HeidiSQL 版本:                  8.0.0.4396
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- 导出 mysite 的数据库结构
DROP DATABASE IF EXISTS `mysite`;
CREATE DATABASE IF NOT EXISTS `mysite` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `mysite`;


-- 导出  表 mysite.category 结构
DROP TABLE IF EXISTS `category`;
CREATE TABLE IF NOT EXISTS `category` (
  `id` int(4) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- 正在导出表  mysite.category 的数据：0 rows
DELETE FROM `category`;
/*!40000 ALTER TABLE `category` DISABLE KEYS */;
/*!40000 ALTER TABLE `category` ENABLE KEYS */;


-- 导出  表 mysite.message 结构
DROP TABLE IF EXISTS `message`;
CREATE TABLE IF NOT EXISTS `message` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `title` varchar(50) DEFAULT NULL,
  `content` text,
  `user_id` int(11) NOT NULL,
  `pubdate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

-- 正在导出表  mysite.message 的数据：11 rows
DELETE FROM `message`;
/*!40000 ALTER TABLE `message` DISABLE KEYS */;
INSERT INTO `message` (`id`, `title`, `content`, `user_id`, `pubdate`) VALUES
	(1, '夏天到了', '天气好热度啊', 2, '2017-05-19 08:43:33'),
	(2, '最近有什么好电影吗', '想看电影了，谁推荐几个电影', 3, '2017-05-19 08:44:03'),
  (3, '再见', '再见啊', 2, '2017-05-19 08:44:03'),
  (4, '作业好难', '求解救，求帮忙', 4, '2017-05-19 08:44:03'),
	(5, '最近好烦', '交友，wx:tosdfdt', 5, '2017-05-19 08:44:03'),
	(6, '讲个笑话', '从前有座山，后来....', 6, '2017-05-19 08:44:03'),
	(7, '明天又放放假了', '约电影，约饭', 4, '2017-05-19 08:44:03'),
	(8, '异星觉醒', '卡尔文好可怕。吓人。', 2, '2017-05-25 23:32:55'),
	(9, '剧荒', '好久没有追剧了。不知道什么好看', 3, '2017-05-25 23:33:31'),
	(10, '我也不知道', '和我有什么关系', 4, '2017-05-25 23:33:56'),
	(11, '天天', '测试留言', 5, '2017-05-25 23:34:14'),
	(12, '怎么不好玩', '这个是怎么实现的啊', 6, '2017-05-25 23:34:31');
/*!40000 ALTER TABLE `message` ENABLE KEYS */;


-- 导出  视图 mysite.message_detail_view 结构
DROP VIEW IF EXISTS `message_detail_view`;
-- 创建临时表以解决视图依赖性错误
CREATE TABLE `message_detail_view` (
	`id` INT(10) NOT NULL,
	`title` VARCHAR(50) NULL COLLATE 'utf8_general_ci',
	`content` TEXT NULL COLLATE 'utf8_general_ci',
	`user_id` INT(11) NOT NULL,
	`pubdate` TIMESTAMP NOT NULL,
	`username` VARCHAR(255) NULL COLLATE 'utf8_general_ci',
	`nickname` VARCHAR(50) NULL COLLATE 'utf8_general_ci'
) ENGINE=MyISAM;


-- 导出  表 mysite.post 结构
DROP TABLE IF EXISTS `post`;
CREATE TABLE IF NOT EXISTS `post` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `title` varchar(50) DEFAULT NULL,
  `content` text,
  `category_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `pubdate` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- 正在导出表  mysite.post 的数据：0 rows
DELETE FROM `post`;
/*!40000 ALTER TABLE `post` DISABLE KEYS */;
/*!40000 ALTER TABLE `post` ENABLE KEYS */;


-- 导出  表 mysite.user 结构
DROP TABLE IF EXISTS `user`;
CREATE TABLE IF NOT EXISTS `user` (
  `id` int(4) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) DEFAULT NULL,
  `password` varchar(32) DEFAULT NULL,
  `nickname` varchar(50) DEFAULT NULL,
  `admin` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- 正在导出表  mysite.user 的数据：6 rows
DELETE FROM `user`;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` (`id`, `username`, `password`, `nickname`, `admin`) VALUES
	(1, 'admin', 'admin', '管理员', 1),
	(2, 'john', '123', '会飞的鱼', 0),
	(3, 'jane', '123', 'MC子龙', 0),
	(4, 'lucy', '123', '花果山的牛魔王', 0),
	(5, 'jim', '123', '银川吴彦祖', 0),
	(6, 'rose', '123', '杂碎西施', 0);
/*!40000 ALTER TABLE `user` ENABLE KEYS */;


-- 导出  视图 mysite.message_detail_view 结构
DROP VIEW IF EXISTS `message_detail_view`;
-- 移除临时表并创建最终视图结构
DROP TABLE IF EXISTS `message_detail_view`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` VIEW `mysite`.`message_detail_view` AS SELECT message.*, user.username, user.nickname
FROM message INNER JOIN user
on message.user_id = user.id ;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;

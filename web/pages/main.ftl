<!doctype html>
<html lang="zh">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <script src="/scripts/jquery.js"></script>
    <script src="/scripts/semantic.min.js"></script>
    <link rel="stylesheet" href="/themes/semantic.min.css">
</head>
<style>
    html body {
        height: 100%;
    }

    #main-container {
        min-height: 90%;

    }
</style>

<body>

<div class="ui inverted stackable menu">
    <div class="ui container">
        <div class="header item">
            <a href="/">首页</a>
        </div>

        <div class="item">
            <a href="/message/list">留言列表</a>
        </div>

        <div class="item">
            <div class="ui icon input">
                <i class="search icon"></i>
                <input type="text" name="searchcontent" placeholder="搜索...">
            </div>
        </div>

    <#if (session.user)??>
        <div class="right item">
            <i class="user icon"></i>
        ${(session.user.nickname)!''}</div>
        <div class="item"><a href="/logout">登出</a></div>
    <#else>
        <div class="right item"><a href="/login">登录</a></div>
        <div class="item"><a href="/login">注册</a></div>
    </#if>

    </div>
</div> <!-- end menu -->

<div class="ui grid container" id="main-container">
    <div class="twelve wide column">
        <div class="ui segment">正文</div>
    </div>
    <div class="four wide column">
        <div class="ui segment">侧边</div>
    </div>

</div>

<div class="ui vertical inverted footer segment">
    <div class="ui center aligned text container">
        宁夏大学信息工程学院 版权所有 2017－2020
    </div>
</div>

</body>
</html>

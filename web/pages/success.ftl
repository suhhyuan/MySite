<!doctype html>
<html lang="zh">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <title>登录成功</title>
</head>
<body>
欢迎你 ${session.user!''}，登录成功。
<a href="/message/list">显示留言</a>
<a href="/message/create">创建留言</a>
</body>
</html>
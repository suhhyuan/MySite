<#-- @ftlvariable name="messages" type="java.util.List<my.model.Message>" -->
<#-- @ftlvariable name="message" type="my.model.Message" -->
<!doctype html>
<html lang="zh">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <script src="/scripts/jquery.js"></script>
    <script src="/scripts/semantic.min.js"></script>
    <script src="/scripts/jquery.dataTables.min.js"></script>
    <script src="/scripts/dataTables.semanticui.min.js"></script>
    <link rel="stylesheet" href="/themes/semantic.min.css">
    <link rel="stylesheet" href="/themes/dataTables.semanticui.min.css">

</head>
<style>
    html body {
        height: 100%;
    }

    #main-container {
        min-height: 90%;

    }
</style>

<body>

<div class="ui inverted stackable menu">
    <div class="ui container">
        <div class="header item">
            <a href="/">首页</a>
        </div>

        <div class="item">
            <a href="/message/list">留言列表</a>
        </div>

        <div class="item">
            <div class="ui icon input">
                <i class="search icon"></i>
                <input type="text" name="searchcontent" placeholder="搜索...">
            </div>
        </div>

    <#if (session.user)??>
        <div class="right item">
            <i class="user icon"></i>
        ${(session.user.nickname)!''}</div>
        <div class="item"><a href="/logout">登出</a></div>
    <#else>
        <div class="right item"><a href="/login">登录</a></div>
        <div class="item"><a href="/login">注册</a></div>
    </#if>

    </div>
</div> <!-- end menu -->

<div class="ui grid container" id="main-container">
    <div class="sixteen wide column">
        <div class="ui segment">
            <table class="ui celled table" id="message-table">
                <thead>
                <tr>
                    <th>序号</th>
                    <th class="two wide">用户</th>
                    <th class="four wide">标题</th>
                    <th class="four wide">内容</th>
                    <th>发布时间</th>
                    <th>操作</th>
                </tr>
                </thead>
                <tbody>
                <#if messages??>
                    <#list messages as message>
                    <tr>
                        <td>${message.id}</td>
                        <td>${message.nickname}</td>
                        <td>${message.title}</td>
                        <td>${message.content}</td>
                        <td>${message.pubdate}</td>
                        <td>
                            <#if (session.user)?? && session.user.id == message.user_id>
                                <a class="ui blue icon edit button" data-id="${message.id}">
                                    <i class="edit icon"></i>
                                    编辑
                                </a>
                                <a class="ui red icon delete button" data-id="${message.id}">
                                    <i class="delete icon"></i>删除</a>
                            </#if>
                        </td>
                    </tr>
                    </#list>
                </#if>
                </tbody>

            </table>


        </div>
    </div>


</div>

<div class="ui vertical inverted footer segment">
    <div class="ui center aligned text container">
        宁夏大学信息工程学院 版权所有 2017－2020
    </div>
</div>

<div class="ui small modal" id="edit_dialog">
    <i class="close icon"></i>
    <div class="header">编辑留言</div>
    <div class="content">
        <form class="ui form" id="edit-form">
            <div class="field">
                <label for="edit_title">标题</label>
                <input type="text" name="message.title" id="edit_title" placeholder="请输入标题" autocomplete="off">
            </div>
            <div class="field">
                <label for="title">内容</label>
                <textarea name="message.content" id="edit_content"></textarea>
            </div>

            <input type="hidden" name="message.id" id="edit_message_id">
        </form>
    </div>
    <div class="actions">
        <a class="ui approve button" id="save-btn">保存</a>
        <a class="ui cancel button">取消</a>
    </div>
</div>


</body>

<script>
    $(document).ready(function () {
        var dt = $("#message-table").DataTable();

        $(".edit.button").click(
                function () {
                    var id = $(this).attr("data-id");
                    var tr = $(this).parents("tr");
                    var data = dt.row($(this).parents("tr")).data();

                    $("#edit_content").html(data[3]);
                    $("#edit_title").val(data[2]);
                    $("#edit_message_id").val(data[0]);

                    $("#edit_dialog").modal({
                        onApprove: function () {
                            $.ajax({
                                url: "/message/update",
                                data: $("#edit-form").serialize(),

                                type: "POST",
                                successTest: function (data) {
                                    return data.success || false;
                                }

                                ,
                                onSuccess: function (data) {
                                    window.location.reload();
                                }
                            });
                        }


                    }).modal("show");
                });
    });


</script>
</html>

<!doctype html>
<html lang="zh">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <script src="/scripts/jquery.js" type="text/javascript"></script>
    <script src="/scripts/jquery.serialize-object.min.js" type="text/javascript"></script>
    <script src="/scripts/semantic.min.js" type="text/javascript"></script>
    <link rel="stylesheet" href="/themes/semantic.min.css">
    <title>登录</title>

</head>
<body>
<div class="ui stackable middle aligned centered grid container">
    <div class="ui row" style="margin-top: 50px;">
        <div class="ui six wide column">
            <div class="ui centered header"><h1>登录</h1></div>
        </div>
    </div>

    <div class="ui six wide column">
        <form method="post" class="ui form" id="login-form">
            <div class="ui segment">
                <div class="ui error message"></div>
                <div class="field">
                    <div class="ui fluid left icon input">
                        <input type="text" name="username" placeholder="用户名" autocomplete="off"/>
                        <i class="user icon"></i>
                    </div>
                </div>

                <div class="field">
                    <div class="ui fluid left icon input">
                        <input type="password" name="password" placeholder="密码" autocomplete="off"/>
                        <i class="lock icon"></i>
                    </div>
                </div>
                <input type="hidden" value="${captcha!''}"/>
                <a class="ui fluid green submit button">登录</a>
            </div>
        </form>
    </div>
    <div class="row">
        <div class="six wide column">
            <div class="ui message">
                还没有注册，点击<a href="/register">注册</a>一个新用户
            </div>
        </div>
    </div>
</div>

</body>
<script>
    $("#login-form").form(
            {
                fields: {
                    username: {
                        identifier: "username",
                        rules: [{
                            type: "empty",
                            prompt: "用户名不能为空"
                        }]
                    }, password: {
                        identifier: "password",
                        rules: [{
                            type: "empty",
                            prompt: "密码不能为空"
                        }]
                    }
                }
            })
            .api({
                url: "/loginCheck",
                serializeForm: true,
                beforeSend: function (data) {
                    $("#login-form").addClass('loading');
                    return data;
                },
                successTest: function (data) {
                    return data.success || false;
                },
                onSuccess: function (data) {
                    window.location.href = "/main";
                },
                onFailure: function (data) {
                    $("#login-form").removeClass('loading');

                    $("#login-form").form('add errors', [data.message]);
                    $("#login-form").form('clear');

                }
            });
</script>
</html>